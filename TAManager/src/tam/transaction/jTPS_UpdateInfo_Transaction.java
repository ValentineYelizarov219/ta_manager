package tam.transaction;

import tam.data.TAData;
import tam.data.TeachingAssistant;
import jtps.jTPS_Transaction;
import javafx.scene.control.TableView;

/**
 *
 * @author Valentine Yelizarov
 */
public class jTPS_UpdateInfo_Transaction implements jTPS_Transaction {
    TeachingAssistant ta;
    String oldName, newName, oldEmail, newEmail;
    TAData data;
    TableView table;
    
    public jTPS_UpdateInfo_Transaction(TeachingAssistant ta, String oldName, String newName, String oldEmail, String newEmail, TAData data, TableView table) {
        this.ta = ta;
        this.oldName = oldName;
        this.newName = newName;
        this.oldEmail = oldEmail;
        this.newEmail = newEmail;
        this.data = data;
        this.table = table;
    }
    
    public void undoTransaction()
    {
        ta.setName(oldName);
        ta.setEmail(oldEmail);
        data.updateNameChange(newName, oldName);
        table.refresh();
    }
    
    public void doTransaction()
    {
        ta.setName(newName);
        ta.setEmail(newEmail);
        data.updateNameChange(oldName, newName);
        table.refresh();
    }
    
    
}
