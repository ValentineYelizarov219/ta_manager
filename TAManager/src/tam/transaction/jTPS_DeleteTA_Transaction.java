/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.transaction;

import tam.data.TAData;
import tam.data.TeachingAssistant;
import jtps.jTPS_Transaction;
import javafx.scene.control.TableView;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class jTPS_DeleteTA_Transaction implements jTPS_Transaction {
    ArrayList<String> list;
    TAData data;
    TeachingAssistant ta;
    TableView table;
    
    public jTPS_DeleteTA_Transaction(TeachingAssistant ta, TAData data, TableView table) {
        list = new ArrayList();
        this.ta = ta;
        this.data = data;
        this.table = table;
    }
    
    public void doTransaction()
    {
        table.getItems().remove(ta);
        
        // REMOVE FROM TIME SLOTS
        for (int row = 1; row < data.getNumRows(); row++) {
            for (int col = 2; col < 7; col++) {
                String cellKey = data.getCellKey(col, row);
                String timeSlotText = data.getOfficeHours().get(cellKey).getValue();
                if(timeSlotText.contains(ta.getName()))
                {
                    list.add(cellKey);
                    data.toggleTAOfficeHours(cellKey, ta.getName());
                }
            }
        }
    }
    
    public void undoTransaction()
    {
        table.getItems().add(ta);
        
        for(int i = 0; i < list.size(); i++)
        {
            data.toggleTAOfficeHours(list.get(i), ta.getName());
        }
    }
}
