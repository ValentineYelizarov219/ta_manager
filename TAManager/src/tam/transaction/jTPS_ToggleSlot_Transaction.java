package tam.transaction;

import jtps.jTPS_Transaction;
import tam.data.TAData;

/**
 *
 * @author Valentine Yelizarov
 */
public class jTPS_ToggleSlot_Transaction implements jTPS_Transaction {
    TAData data;
    String cellKey, taName;
    
    public jTPS_ToggleSlot_Transaction(TAData data, String cellKey, String taName)
    {
        this.data = data;
        this.cellKey = cellKey;
        this.taName = taName;
    }
    
    public void doTransaction()
    {
        // TOGGLE THE OFFICE HOURS IN THE CLICKED CELL
        data.toggleTAOfficeHours(cellKey, taName);
    }
    
    public void undoTransaction()
    {
        // TOGGLE THE OFFICE HOURS IN THE CLICKED CELL
        data.toggleTAOfficeHours(cellKey, taName);
    }
}
