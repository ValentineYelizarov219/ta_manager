/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.transaction;
import jtps.jTPS_Transaction;
import tam.data.TAData;
import java.util.HashMap;

/**
 *
 * @author Valentine Yelizarov
 */
public class jTPS_ResizeTime_Transaction implements jTPS_Transaction {
    TAData data;
    String time;
    HashMap officeHours;
    int startHour, endHour;
    boolean type;
    
    public jTPS_ResizeTime_Transaction(boolean type, TAData data, String time, int startHour, int endHour)
    {
        this.data = data;
        this.time = time;
        officeHours = (HashMap)data.getOfficeHours().clone();
        this.startHour = startHour;
        this.endHour = endHour;
        this.type = type;
    }
    
    public void doTransaction()
    {
        if(type)
            data.expandBottom(time);
        else
            data.expandTop(time);
    }
    
    public void undoTransaction()
    {
        data.undoExpand(startHour, endHour, officeHours);
    }
}
