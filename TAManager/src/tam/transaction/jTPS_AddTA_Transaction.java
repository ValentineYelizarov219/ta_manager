package tam.transaction;

import jtps.jTPS_Transaction;
import tam.data.TAData;
import tam.data.TeachingAssistant;

/**
 *
 * @author Valentine Yelizarov
 */
public class jTPS_AddTA_Transaction implements jTPS_Transaction {
    TAData data;
    String name, email;
    
    public jTPS_AddTA_Transaction(TAData data, String name, String email)
    {
        this.data = data;
        this.name = name;
        this.email = email;
    }
    
    public void doTransaction()
    {
        data.addTA(name, email);
    }
    
    public void undoTransaction()
    {
        data.getTeachingAssistants().remove(data.getTA(name));
    }
}
