package tam.data;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import djf.components.AppDataComponent;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoCancelDialogSingleton;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;
import javafx.beans.property.StringProperty;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.TAManagerProp;
import static tam.TAManagerProp.RESIZE_ERROR_MESSAGE;
import static tam.TAManagerProp.RESIZE_ERROR_TITLE;
import static tam.TAManagerProp.RESIZE_WORKSPACE_MESSAGE;
import static tam.TAManagerProp.RESIZE_WORKSPACE_TITLE;
import tam.workspace.TAWorkspace;

/**
 * This is the data component for TAManagerApp. It has all the data needed
 * to be set by the user via the User Interface and file I/O can set and get
 * all the data from this object
 * 
 * @author Richard McKenna
 * @author Valentine Yelizarov
 */
public class TAData implements AppDataComponent {

    // WE'LL NEED ACCESS TO THE APP TO NOTIFY THE GUI WHEN DATA CHANGES
    TAManagerApp app;

    // NOTE THAT THIS DATA STRUCTURE WILL DIRECTLY STORE THE
    // DATA IN THE ROWS OF THE TABLE VIEW
    ObservableList<TeachingAssistant> teachingAssistants;

    // THIS WILL STORE ALL THE OFFICE HOURS GRID DATA, WHICH YOU
    // SHOULD NOTE ARE StringProperty OBJECTS THAT ARE CONNECTED
    // TO UI LABELS, WHICH MEANS IF WE CHANGE VALUES IN THESE
    // PROPERTIES IT CHANGES WHAT APPEARS IN THOSE LABELS
    HashMap<String, StringProperty> officeHours;
    
    // THESE ARE THE LANGUAGE-DEPENDENT VALUES FOR
    // THE OFFICE HOURS GRID HEADERS. NOTE THAT WE
    // LOAD THESE ONCE AND THEN HANG ON TO THEM TO
    // INITIALIZE OUR OFFICE HOURS GRID
    ArrayList<String> gridHeaders;

    // THESE ARE THE TIME BOUNDS FOR THE OFFICE HOURS GRID. NOTE
    // THAT THESE VALUES CAN BE DIFFERENT FOR DIFFERENT FILES, BUT
    // THAT OUR APPLICATION USES THE DEFAULT TIME VALUES AND PROVIDES
    // NO MEANS FOR CHANGING THESE VALUES
    int startHour;
    int endHour;
    
    // DEFAULT VALUES FOR START AND END HOURS IN MILITARY HOURS
    public static final int MIN_START_HOUR = 9;
    public static final int MAX_END_HOUR = 20;

    /**
     * This constructor will setup the required data structures for
     * use, but will have to wait on the office hours grid, since
     * it receives the StringProperty objects from the Workspace.
     * 
     * @param initApp The application this data manager belongs to. 
     */
    public TAData(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // CONSTRUCT THE LIST OF TAs FOR THE TABLE
        teachingAssistants = FXCollections.observableArrayList();

        // THESE ARE THE DEFAULT OFFICE HOURS
        startHour = MIN_START_HOUR;
        endHour = MAX_END_HOUR;
        
        //THIS WILL STORE OUR OFFICE HOURS
        officeHours = new HashMap();
        
        // THESE ARE THE LANGUAGE-DEPENDENT OFFICE HOURS GRID HEADERS
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ArrayList<String> timeHeaders = props.getPropertyOptionsList(TAManagerProp.OFFICE_HOURS_TABLE_HEADERS);
        ArrayList<String> dowHeaders = props.getPropertyOptionsList(TAManagerProp.DAYS_OF_WEEK);
        gridHeaders = new ArrayList();
        gridHeaders.addAll(timeHeaders);
        gridHeaders.addAll(dowHeaders);
    }
    
    /**
     * Called each time new work is created or loaded, it resets all data
     * and data structures such that they can be used for new values.
     */
    @Override
    public void resetData() {
        startHour = MIN_START_HOUR;
        endHour = MAX_END_HOUR;
        teachingAssistants.clear();
        officeHours.clear();
    }
    
    // ACCESSOR METHODS

    public int getStartHour() {
        return startHour;
    }

    public int getEndHour() {
        return endHour;
    }
    
    public ArrayList<String> getGridHeaders() {
        return gridHeaders;
    }

    public ObservableList getTeachingAssistants() {
        return teachingAssistants;
    }
    
    public String getCellKey(int col, int row) {
        return col + "_" + row;
    }

    public StringProperty getCellTextProperty(int col, int row) {
        String cellKey = getCellKey(col, row);
        return officeHours.get(cellKey);
    }

    public HashMap<String, StringProperty> getOfficeHours() {
        return officeHours;
    }
    
    public int getNumRows() {
        return ((endHour - startHour) * 2) + 1;
    }

    public String getTimeString(int militaryHour, boolean onHour) {
        String minutesText = "00";
        if (!onHour) {
            minutesText = "30";
        }

        // FIRST THE START AND END CELLS
        int hour = militaryHour;
        if (hour > 12) {
            hour -= 12;
        }
        String cellText = "" + hour + ":" + minutesText;
        if (militaryHour < 12) {
            cellText += "am";
        } else {
            cellText += "pm";
        }
        return cellText;
    }
    
    public String getCellKey(String day, String time) {
        int col = gridHeaders.indexOf(day);
        int row = 1;
        int hour = Integer.parseInt(time.substring(0, time.indexOf("_")));
        int milHour = hour;
        if (hour < startHour)
            milHour += 12;
        row += (milHour - startHour) * 2;
        if (time.contains("_30"))
            row += 1;
        return getCellKey(col, row);
    }
    
    public TeachingAssistant getTA(String testName) {
        for (TeachingAssistant ta : teachingAssistants) {
            if (ta.getName().equals(testName)) {
                return ta;
            }
        }
        return null;
    }
    
    public TeachingAssistant getTAEmail(String testEmail) {
        for (TeachingAssistant ta : teachingAssistants) {
            if (ta.getEmail().equals(testEmail)) {
                return ta;
            }
        }
        return null;
    }
    
    /**
     * This method is for giving this data manager the string property
     * for a given cell.
     */
    public void setCellProperty(int col, int row, StringProperty prop) {
        String cellKey = getCellKey(col, row);
        officeHours.put(cellKey, prop);
    }    
    
    /**
     * This method is for setting the string property for a given cell.
     */
    public void setGridProperty(ArrayList<ArrayList<StringProperty>> grid,
                                int column, int row, StringProperty prop) {
        grid.get(row).set(column, prop);
    }
    
    private void initOfficeHours(int initStartHour, int initEndHour) {
        // NOTE THAT THESE VALUES MUST BE PRE-VERIFIED
        startHour = initStartHour;
        endHour = initEndHour;
        
        // EMPTY THE CURRENT OFFICE HOURS VALUES
        officeHours.clear();
            
        // WE'LL BUILD THE USER INTERFACE COMPONENT FOR THE
        // OFFICE HOURS GRID AND FEED THEM TO OUR DATA
        // STRUCTURE AS WE GO
        TAWorkspace workspaceComponent = (TAWorkspace)app.getWorkspaceComponent();
        workspaceComponent.reloadOfficeHoursGrid(this);
    }
    
    public void reInit(int oldStartTime, int oldEndTime)
    {
        startHour = oldStartTime;
        endHour = oldEndTime;
        TAWorkspace workspaceComponent = (TAWorkspace)app.getWorkspaceComponent();
        workspaceComponent.reloadOfficeHoursGrid(this);
    }
    
    public void initHours(String startHourText, String endHourText) {
        int initStartHour = Integer.parseInt(startHourText);
        int initEndHour = Integer.parseInt(endHourText);
        if ((initStartHour <= initEndHour)) {
            // THESE ARE VALID HOURS SO KEEP THEM
            initOfficeHours(initStartHour, initEndHour);
        }
    }

    public boolean containsTA(String testName) {
        for (TeachingAssistant ta : teachingAssistants) {
            if (ta.getName().equals(testName)) {
                return true;
            }
        }
        return false;
    }
    
    public boolean containsTAIgnore(String testName, TeachingAssistant t) {
        for (TeachingAssistant ta : teachingAssistants) {
            if (ta.getName().equals(testName) && ta != t) {
                return true;
            }
        }
        return false;
    }

    public boolean containsTAEmail(String testEmail) {
        for (TeachingAssistant ta : teachingAssistants) {
            if (ta.getEmail().equals(testEmail)) {
                return true;
            }
        }
        return false;
    }

    public boolean containsTAEmailIgnore(String testEmail, TeachingAssistant t) {
        for (TeachingAssistant ta : teachingAssistants) {
            if (ta.getEmail().equals(testEmail) && ta != t) {
                return true;
            }
        }
        return false;
    }
    
    public void setOfficeHours(HashMap h)
    {
        officeHours = h;
    }

    public void addTA(String initName, String initEmail) {
        // MAKE THE TA
        TeachingAssistant ta = new TeachingAssistant(initName, initEmail);

        // ADD THE TA
        if (!containsTA(initName)) {
            teachingAssistants.add(ta);
        }

        // SORT THE TAS
        Collections.sort(teachingAssistants);
    }

    public void addOfficeHoursReservation(String day, String time, String taName) {
        String cellKey = getCellKey(day, time);
        toggleTAOfficeHours(cellKey, taName);
    }

    public void setReservation(String cellKey, String taNames) {
        toggleTAOfficeHours(cellKey, taNames);
    }
    
    /**
     * This function toggles the taName in the cell represented
     * by cellKey. Toggle means if it's there it removes it, if
     * it's not there it adds it.
     */
    public void toggleTAOfficeHours(String cellKey, String taName) {
        StringProperty cellProp = officeHours.get(cellKey);
        String cellText = cellProp.getValue();
        if (!cellText.contains(taName))
        {
            if(cellProp.getValue().equals(""))
                cellProp.setValue(cellText + taName);
            else
                cellProp.setValue(cellText + "\n" + taName);
        }
        else
            removeTAFromCell(cellProp, taName);
    }
    
    /**
     * This method removes taName from the office grid cell
     * represented by cellProp.
     */
    public void removeTAFromCell(StringProperty cellProp, String taName) {
        // GET THE CELL TEXT
        String cellText = cellProp.getValue();
        // IS IT THE ONLY TA IN THE CELL?
        if (cellText.equals(taName)) {
            cellProp.setValue("");
        }
        // IS IT THE FIRST TA IN A CELL WITH MULTIPLE TA'S?
        else if (cellText.indexOf(taName) == 0) {
            int startIndex = cellText.indexOf("\n") + 1;
            cellText = cellText.substring(startIndex, cellText.length());
            cellProp.setValue(cellText);
        }
        // IT MUST BE ANOTHER TA IN THE CELL
        else {
            int startIndex = cellText.indexOf("\n" + taName);
            
            cellText = cellText.substring(0, startIndex) + cellText.substring(startIndex + taName.length() + 1, cellText.length());
            cellProp.setValue(cellText);
        }
    }
    
    /**
     * Replaces all instances of a name with a different name in the grid
     * 
     * @param from  name to change from
     * @param to    name to change to
     */
    public void updateNameChange(String from, String to)
    {
        Set<String> set = officeHours.keySet();
        for(String key : set)
        {
            StringProperty sp = officeHours.get(key);
            String temp = sp.getValue();
            sp.setValue(temp.replace(from, to));
        }
    }
    
    public boolean checkEmpty(int oldTime, int newTime)
    {
        boolean ret = false;
        if(oldTime == startHour)
        {
            if(newTime <= startHour)
                ret = false;
            else
            {
                int fromRow = 1 + (newTime - newTime) * 2;
                int toRow = 1 + (newTime - oldTime) * 2;
                
                for(int i = fromRow; i < toRow && !ret; i++)
                {
                    for(int j = 2; j <= 6 && !ret; j++)
                    {
                        if(!officeHours.get(getCellKey(j, i)).getValue().isEmpty())
                        {
                            ret = true;
                        }
                    }
                }
            }
        }
        else
        {
            if(newTime >= endHour)
                ret = false;
            else
            {
                int fromRow = (oldTime - startHour) * 2;
                int toRow = (newTime - startHour) * 2;
                for(int i = fromRow; i > toRow && !ret; i--)
                {
                    for(int j = 2; j <= 6 && !ret; j++)
                    {
                        if(!officeHours.get(getCellKey(j, i)).getValue().isEmpty())
                        {
                            ret = true;
                        }
                    }
                }
            }
        }
        return ret;
    }
    
    public void copyData(HashMap<String, StringProperty> h)
    {
        for(String key : (Set<String>)h.keySet())
        {
            StringProperty prop = h.get(key);
            if(officeHours.get(key) != null)
            {
                officeHours.get(key).setValue(prop.getValue());
            }
        }
    }
    
    public void copyData(HashMap<String, StringProperty> h, int offset)
    {
        for(String key : (Set<String>)h.keySet())
        {
            StringProperty prop = h.get(key);
            String temp = key.substring(key.indexOf("_") + 1); //row
            String temp2 = key.substring(0, key.indexOf("_")); //col
            if(!temp.equals("0") && (!temp2.equals("1") || !temp2.equals("0")))
            {
                temp = Integer.toString(Integer.parseInt(temp) + offset);
                key = key.substring(0, key.indexOf("_") + 1) + temp;
                if(officeHours.get(key) != null && !key.substring(key.indexOf("_") + 1).equals("0"))
                {
                    officeHours.get(key).setValue(prop.getValue());
                }
            }
        }
    }
    
    public void expandBottom(String s1)
    {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String time = s1.substring(0, s1.indexOf(":"));               
        if(s1.substring(s1.length()-2).equals("pm") && !time.equals("12")
                ||s1.substring(s1.length()-2).equals("am") && time.equals("12"))
        {
            time = Integer.toString((Integer.parseInt(time) + 12));
        }
        if(Integer.parseInt(time) < getEndHour())
        {
            HashMap h = (HashMap<String, StringProperty>)getOfficeHours().clone();
            int oldTime = getStartHour();
            int difference = oldTime - Integer.parseInt(time);
            System.out.println(oldTime);
            boolean agree = true;
            if(checkEmpty(oldTime, Integer.parseInt(time)))
            {
                agree = false;
                AppYesNoCancelDialogSingleton win = AppYesNoCancelDialogSingleton.getSingleton();
                win.show(props.getProperty(RESIZE_WORKSPACE_TITLE), props.getProperty(RESIZE_WORKSPACE_MESSAGE));
                agree = win.getSelection().equals(AppYesNoCancelDialogSingleton.YES);
            }

            if(agree)
            {
                app.getWorkspaceComponent().resetWorkspace();
                initHours(time, Integer.toString(getEndHour()));
                copyData(h, difference*2);
            }
        }
        else
        {
            AppMessageDialogSingleton message = AppMessageDialogSingleton.getSingleton();
            message.show(props.getProperty(RESIZE_ERROR_TITLE), props.getProperty(RESIZE_ERROR_MESSAGE));
        }
    }
    
    public void expandTop(String s1)
    {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String time = s1.substring(0, s1.indexOf(":"));               
        if(s1.substring(s1.length()-2).equals("pm") && !time.equals("12")
                ||s1.substring(s1.length()-2).equals("am") && time.equals("12"))
        {
            time = Integer.toString((Integer.parseInt(time) + 12));
        }
        if(Integer.parseInt(time) > getStartHour())
        {
            HashMap h = (HashMap<String, StringProperty>)getOfficeHours().clone();
            int oldTime = getEndHour();
            System.out.println(oldTime);
            boolean agree = true;
            if(checkEmpty(oldTime, Integer.parseInt(time)))
            {
                agree = false;
                AppYesNoCancelDialogSingleton win = AppYesNoCancelDialogSingleton.getSingleton();
                win.show(props.getProperty(RESIZE_WORKSPACE_TITLE), props.getProperty(RESIZE_WORKSPACE_MESSAGE));
                agree = win.getSelection().equals(AppYesNoCancelDialogSingleton.YES);
            }

            if(agree)
            {
                app.getWorkspaceComponent().resetWorkspace();
                initHours(Integer.toString(getStartHour()), time);
                copyData(h);
            }
        }
        else
        {
            AppMessageDialogSingleton message = AppMessageDialogSingleton.getSingleton();
            message.show(props.getProperty(RESIZE_ERROR_TITLE), props.getProperty(RESIZE_ERROR_MESSAGE));
        }
    }
    
    public void undoExpand(int timeStart, int timeEnd, HashMap h)
    {
        officeHours.clear();
            
        TAWorkspace workspaceComponent = (TAWorkspace)app.getWorkspaceComponent();
        workspaceComponent.resetWorkspace();
        initOfficeHours(timeStart, timeEnd);
        
        for(String key : (Set<String>)h.keySet())
        {
            String text = ((StringProperty)(((HashMap<String,StringProperty>)h).get(key))).getValue();
            officeHours.get(key).setValue(text);
        }
    }
}